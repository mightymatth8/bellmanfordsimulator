package hr.fer.tel.infmre.bellmanford;

import javax.swing.SwingUtilities;

import hr.fer.tel.infmre.bellmanford.gui.UserInterface;

public class BellmanFordSimulator {

    public static void main(String[] args) {
    	SwingUtilities.invokeLater(() -> {
            new UserInterface().setVisible(true);
        });
    }

}
